<?php

namespace tests\Fidesio\DockerfileComposer\Command;

use Fidesio\DockerfileComposer\Command\DockerfileCommand;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use org\bovigo\vfs\visitor\vfsStreamStructureVisitor;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class DockerfileCommandTest extends TestCase
{
    /** @var CommandTester */
    private $commandTester;

    /** @var vfsStreamDirectory */
    private $vfsStream;

    protected function setUp(): void
    {
        $application = new Application();
        $application->add(new DockerfileCommand());
        $this->commandTester = new CommandTester($application->get('generate'));

        $this->vfsStream = vfsStream::setup();
    }

    protected function tearDown(): void
    {
        $this->commandTester = null;
    }

    public function testGenerate(): void
    {
        $configPath     = dirname(__DIR__, 3) . '/fixtures/php71.config.json';
        $dockerfilePath = dirname(__DIR__, 3) . '/fixtures/php71.Dockerfile';

        $config     = file_get_contents($configPath);
        $dockerfile = file_get_contents($dockerfilePath);

        vfsStream::create(
            [
                'config.json' => $config,
                'output'      => [],
            ]
        );

        $this->commandTester->execute(
            [
                'config' => $this->vfsStream->getChild('config.json')->url(),
                'output' => $this->vfsStream->getChild('output')->url(),
            ]
        );

        $this->assertFileEquals($dockerfilePath, $this->vfsStream->getChild('output/Dockerfile')->url());

        $this->assertEquals(
            [
                'root' => [
                    'config.json' => $config,
                    'output'      => [
                        'Dockerfile' => $dockerfile,
                    ],
                ],
            ],
            (new vfsStreamStructureVisitor())->visitDirectory($this->vfsStream)->getStructure()
        );
    }
}
