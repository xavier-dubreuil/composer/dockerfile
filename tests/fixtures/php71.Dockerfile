FROM php:7.1-fpm-stretch
LABEL Description="Contains PHP 7.1 FPM + Node 10.15.1" \
      Licence="proprietary" \
      maintainer="Example <docker@example.com>"
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections && \
    apt-get update && apt-get upgrade -y apt-utils && \
    apt-get install -y --no-install-recommends git \
    nano \
    tree \
    vim \
    curl \
    ftp \
    wget \
    unzip \
    zip \
    redis-tools \
    gnupg \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    libssl-dev \
    libcurl4-openssl-dev \
    libxml2-dev \
    libmagickwand-dev && \
    docker-php-ext-configure gd --with-freetype-dir=/usr/include/ \
    --with-jpeg-dir=/usr/include/ && \
    docker-php-ext-install -j$(nproc) gd && \
    docker-php-ext-install curl && \
    docker-php-ext-install iconv && \
    docker-php-ext-install mbstring && \
    docker-php-ext-install hash && \
    docker-php-ext-install json && \
    docker-php-ext-install pdo_mysql && \
    docker-php-ext-install soap && \
    pecl install imagick-3.4.3 && \
    docker-php-ext-enable imagick && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer && \
    curl -sL https://deb.nodesource.com/setup_10.x | /bin/bash - && \
    apt-get install -y --no-install-recommends nodejs && \
    npm install -g n && \
    npm install -g npm@6.4.1 && \
    n 10.15.2 && \
    npm install -g gulp grunt-cli yarn && \
    apt-get purge -y --auto-remove && \
    apt-get clean

COPY composer.json /var/www/html/composer.json
COPY composer.lock /var/www/html/composer.lock
COPY symfony.lock /var/www/html/symfony.lock

RUN composer install --no-dev --no-progress --no-suggest --optimize-autoloader --no-ansi --no-interaction

WORKDIR /var/www/html

VOLUME ["/var/log", "/var/sessions"]
