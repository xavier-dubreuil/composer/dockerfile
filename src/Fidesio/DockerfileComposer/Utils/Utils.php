<?php

namespace Fidesio\DockerfileComposer\Utils;

/**
 * Class Utils
 * @package Fidesio\DockerfileComposer\Utils
 */
class Utils
{
    /**
     * @param string $key
     * @param array  $search
     *
     * @return bool
     */
    public static function keyExist(string $key, array $search): bool
    {
        if (!is_array($search)) {
            return false;
        }

        if (array_key_exists($key, $search)) {
            return true;
        }

        foreach ($search as $subSearch) {
            if (is_array($subSearch) && self::keyExist($key, $subSearch)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $value
     * @param array  $search
     *
     * @return bool
     */
    public function inArray(string $value, array $search): bool
    {
        if (!is_array($search)) {
            return false;
        }

        if (in_array($value, $search, true)) {
            return true;
        }

        foreach ($search as $subSearch) {
            if (is_array($subSearch) && self::inArray($value, $subSearch)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param array  $commands
     * @param string $instruction
     * @param string $endLine
     *
     * @return array
     */
    public static function multilines(array $commands, string $instruction, string $endLine): array
    {
        $lines   = [];
        $endLine = $endLine !== '' ? ' ' . $endLine : $endLine;

        foreach ($commands as $index => $command) {
            $line = '';
            $line .= $index === 0 ? $instruction . ' ' :
                str_repeat(' ', count(range(0, strlen($instruction))));
            $line .= $command;

            if ($index < count($commands) - 1) {
                $line .= $endLine . ' \\';
            }

            $lines [] = $line;
        }

        return $lines;
    }
}
