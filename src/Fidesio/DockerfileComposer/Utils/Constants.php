<?php

namespace Fidesio\DockerfileComposer\Utils;

/**
 * Class Constants
 * @package Fidesio\DockerfileComposer\Utils
 */
class Constants
{
    public const  APACHE        = 'apache';
    public const  DOCUMENT_ROOT = 'document-root';
    public const  MODULES       = 'modules';
    public const  APT           = 'apt';
    public const  PACKAGES      = 'packages';
    public const  NODE          = 'node';
    public const  VERSION       = 'version';
    public const  NPM           = 'npm';
    public const  PHP           = 'php';
    public const  PLATFORM      = 'platform';
    public const  EXTENSIONS    = 'extensions';
    public const  DEPENDENCIES  = 'dependencies';
    public const  PLATFORMS     = 'platforms';
    public const  DOCKER        = 'docker';
    public const  LABELS        = 'labels';
    public const  COPY          = 'COPY';
    public const  RUN           = 'RUN';
    public const  ADD           = 'ADD';
    public const  SOURCES       = 'sources';
    public const  TARGET        = 'target';
    public const  WITH_NAME     = 'with-name';
    public const  VOLUME        = 'volume';
    public const  POST_COMMANDS = 'post-commands';
    public const  ENVIRONMENT   = 'environment';
    public const  WORKDIR       = 'workdir';
    public const  IMAGE         = 'image';
    public const  IMAGE_NAME    = 'name';
    public const  OS            = 'os';
    public const  DISTRIBUTION  = 'distribution';
    public const  ENTRYPOINT    = 'entrypoint';
    public const  CMD           = 'cmd';
    public const  EXPOSE        = 'expose';
    public const  COMMANDS      = 'commands';
    public const  ARGUMENTS     = 'arguments';
    public const  DISTRIBUTIONS = ['debian', 'alpine'];
}
