<?php

namespace Fidesio\DockerfileComposer\Interfaces;

/**
 * Interface FactoryInterface
 */
interface FactoryInterface
{
    /**
     * @param string $fileName
     * @param string $outputFile
     */
    public static function generate(string $fileName, string $outputFile): void;
}
