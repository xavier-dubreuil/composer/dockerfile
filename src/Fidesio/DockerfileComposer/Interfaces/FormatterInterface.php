<?php

namespace Fidesio\DockerfileComposer\Interfaces;

/**
 * Interface FormatterInterface
 * @package Fidesio\DockerfileComposer\Interfaces
 */
interface FormatterInterface
{
    /**
     * @return string
     */
    public function format(): string;
}
