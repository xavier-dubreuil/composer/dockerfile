<?php

namespace Fidesio\DockerfileComposer\Interfaces;

use Fidesio\DockerfileComposer\Exception\FileNotCreatedException;
use Fidesio\DockerfileComposer\Exception\ConfigurationMissingException;

/**
 * Interface ConfigurationFileInterface
 * @package Fidesio\DockerfileComposer\Interfaces
 */
interface ConfigurationInterface
{
    /**
     * @return array
     *
     * @throws ConfigurationMissingException
     * @throws FileNotCreatedException
     */
    public function getContents(): array;
}
