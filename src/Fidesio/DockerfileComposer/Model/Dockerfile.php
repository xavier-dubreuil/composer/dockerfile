<?php

namespace Fidesio\DockerfileComposer\Model;

/**
 * Class Dockerfile
 * @package Fidesio\DockerfileComposer\Model
 */
class Dockerfile
{
    /**
     * @var array
     */
    private $from;

    /**
     * @var array
     */
    private $label;

    /**
     * @var array
     */
    private $run;

    /**
     * @var array
     */
    private $cmd;

    /**
     * @var array
     */
    private $expose;

    /**
     * @var array
     */
    private $environment;

    /**
     * @var array
     */
    private $copy;

    /**
     * @var array
     */
    private $entrypoint;

    /**
     * @var array
     */
    private $volume;

    /**
     * @var string
     */
    private $workdir;

    /**
     * @var array
     */
    private $arguments;

    /**
     * Dockerfile constructor.
     */
    private function __construct()
    {
        $this->run         = [];
        $this->from        = [];
        $this->copy        = [];
        $this->label       = [];
        $this->volume      = [];
        $this->environment = [];
        $this->entrypoint  = [];
        $this->cmd         = [];
        $this->expose      = [];
        $this->arguments   = [];
    }

    /**
     * @return self
     */
    public static function create(): self
    {
        return new static();
    }

    /**
     * @return array
     */
    public function getFrom(): array
    {
        return $this->from;
    }

    /**
     * @param array $from
     *
     * @return self
     */
    public function from(array $from): self
    {
        $this->from = $from;

        return $this;
    }

    /**
     * @return array
     */
    public function getLabel(): array
    {
        return $this->label;
    }

    /**
     * @param array $label
     *
     * @return self
     */
    public function label(array $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return array
     */
    public function getEnvironment(): array
    {
        return $this->environment;
    }

    /**
     * @param array $environment
     *
     * @return self
     */
    public function environment(array $environment): self
    {
        $this->environment = $environment;

        return $this;
    }

    /**
     * @return array
     */
    public function getRun(): array
    {
        return $this->run;
    }

    /**
     * @param array $run
     *
     * @return self
     */
    public function run(array $run): self
    {
        $this->run = $run;

        return $this;
    }

    /**
     * @return array
     */
    public function getCopy(): array
    {
        return $this->copy;
    }

    /**
     * @param array $copy
     *
     * @return self
     */
    public function copy(array $copy): self
    {
        $this->copy = $copy;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getWorkdir(): ?string
    {
        return $this->workdir;
    }

    /**
     * @param string $workdir
     *
     * @return self
     */
    public function workdir(string $workdir): self
    {
        $this->workdir = $workdir;

        return $this;
    }

    /**
     * @return array
     */
    public function getVolume(): array
    {
        return $this->volume;
    }

    /**
     * @param array $volume
     *
     * @return self
     */
    public function volume(array $volume): self
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * @return array
     */
    public function getEntrypoint(): array
    {
        return $this->entrypoint;
    }

    /**
     * @param array $entrypoint
     *
     * @return self
     */
    public function entrypoint(array $entrypoint): self
    {
        $this->entrypoint = $entrypoint;

        return $this;
    }

    /**
     * @return array
     */
    public function getCmd(): array
    {
        return $this->cmd;
    }

    /**
     * @param array $cmd
     *
     * @return self
     */
    public function cmd(array $cmd): self
    {
        $this->cmd = $cmd;

        return $this;
    }

    /**
     * @return array
     */
    public function getExpose(): array
    {
        return $this->expose;
    }

    /**
     * @param array $expose
     *
     * @return self
     */
    public function expose(array $expose): self
    {
        $this->expose = $expose;

        return $this;
    }

    /**
     * @return array
     */
    public function getArguments(): array
    {
        return $this->arguments;
    }

    /**
     * @param array $arguments
     *
     * @return self
     */
    public function arguments(array $arguments): self
    {
        $this->arguments = $arguments;

        return $this;
    }
}
