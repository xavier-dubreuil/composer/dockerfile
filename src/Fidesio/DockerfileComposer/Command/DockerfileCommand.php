<?php

namespace Fidesio\DockerfileComposer\Command;

use Fidesio\DockerfileComposer\Exception\FileNotCreatedException;
use Fidesio\DockerfileComposer\Factory\DockerfileFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Fidesio\DockerfileComposer\Exception\ConfigurationMissingException;

/**
 * Class DockerfileCommand
 * @package Fidesio\DockerfileComposer\Command
 */
final class DockerfileCommand extends Command
{
    /**
     * Config command
     *
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->setName('generate')
            ->addArgument('config', InputArgument::REQUIRED, 'Configuration file in format json')
            ->addArgument('output', InputArgument::REQUIRED, 'Directory output')
            ->setDescription('This command will generate a Dockerfile from a json file');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null
     * @throws ConfigurationMissingException
     * @throws FileNotCreatedException
     */
    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        DockerfileFactory::generate(
            (string)$input->getArgument('config'),
            (string)$input->getArgument('output')
        );

        $output->writeln('Successful creation of the Dockerfile file in directory');

        return 0;
    }
}
