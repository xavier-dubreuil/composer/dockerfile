<?php

namespace Fidesio\DockerfileComposer\Handler;

use Fidesio\DockerfileComposer\Exception\ConfigurationMissingException;
use Fidesio\DockerfileComposer\Exception\FileNotCreatedException;
use Fidesio\DockerfileComposer\Model\Dockerfile;
use Fidesio\DockerfileComposer\Utils\Constants;

/**
 * Class DockerfileHandler
 * @package Fidesio\DockerfileComposer\Handler
 */
final class DockerfileHandler
{
    /**
     * @var Dockerfile
     */
    private $dockerfile;

    /**
     * @var array
     */
    private $configuration;

    /**
     * DockerfileHandler constructor.
     */
    private function __construct()
    {
        $this->configuration = [];
        $this->dockerfile    = Dockerfile::create();
    }

    /**
     * @return self
     */
    public static function create(): self
    {
        return new static();
    }

    /**
     * @return array
     */
    public function getConfiguration(): array
    {
        return $this->configuration;
    }

    /**
     * @param string $filename
     *
     * @return Dockerfile
     *
     * @throws ConfigurationMissingException
     * @throws FileNotCreatedException
     */
    public function processConfig(string $filename): Dockerfile
    {
        $this->configuration = Configuration::create($filename)->getContents();

        $this->processInstructionOfBaseImage();
        $this->processInstructionsOfDeclaration();
        $this->processInstructionsOfExecution();
        $this->processAdvancedPackaging();

        return $this->dockerfile;
    }

    /**
     *
     */
    private function processInstructionOfBaseImage(): void
    {
        if (!empty($this->configuration[Constants::PHP])) {
            $this->dockerfile->from($this->configuration[Constants::PHP]);
        }
    }

    /**
     *
     */
    private function processInstructionsOfDeclaration(): void
    {
        if (!empty($this->configuration[Constants::DOCKER][Constants::LABELS])) {
            $this->dockerfile->label($this->configuration[Constants::DOCKER][Constants::LABELS]);
        }
        if (!empty($this->configuration[Constants::DOCKER][Constants::ENVIRONMENT])) {
            $this->dockerfile->environment($this->configuration[Constants::DOCKER][Constants::ENVIRONMENT]);
        }
        if (!empty($this->configuration[Constants::DOCKER][Constants::WORKDIR])) {
            $this->dockerfile->workdir($this->configuration[Constants::DOCKER][Constants::WORKDIR]);
        }
        if (!empty($this->configuration[Constants::DOCKER][Constants::VOLUME])) {
            $this->dockerfile->volume($this->configuration[Constants::DOCKER][Constants::VOLUME]);
        }
        if (!empty($this->configuration[Constants::DOCKER][Constants::ARGUMENTS])) {
            $this->dockerfile->arguments($this->configuration[Constants::DOCKER][Constants::ARGUMENTS]);
        }
    }

    /**
     *
     */
    private function processInstructionsOfExecution(): void
    {
        if (!empty($this->configuration[Constants::DOCKER][Constants::CMD])) {
            $this->dockerfile->cmd($this->configuration[Constants::DOCKER][Constants::CMD]);
        }
        if (!empty($this->configuration[Constants::DOCKER][Constants::EXPOSE])) {
            $this->dockerfile->expose($this->configuration[Constants::DOCKER][Constants::EXPOSE]);
        }
        if (!empty($this->configuration[Constants::DOCKER][Constants::ENTRYPOINT])) {
            $this->dockerfile->entrypoint($this->configuration[Constants::DOCKER][Constants::ENTRYPOINT]);
        }
    }

    /**
     *
     */
    private function processAdvancedPackaging(): void
    {
        if (!empty($this->configuration[Constants::POST_COMMANDS])) {
            $this->dockerfile->copy($this->configuration[Constants::POST_COMMANDS]);
        }
        if (!empty($this->configuration[Constants::OS][Constants::PACKAGES])) {
            $this->dockerfile->run($this->configuration[Constants::OS][Constants::PACKAGES]);
        }
    }
}
