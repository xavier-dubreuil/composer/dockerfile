<?php

namespace Fidesio\DockerfileComposer\Handler;

use Fidesio\DockerfileComposer\Exception\ConfigurationMissingException;
use Fidesio\DockerfileComposer\Utils\Constants;
use Fidesio\DockerfileComposer\Utils\Utils;

/**
 * Class NodeHandler
 * @package Fidesio\DockerfileComposer\Handler
 */
final class NodeHandler extends AbstractHandler
{
    /**
     * @var string
     */
    private $distribution;

    /**
     * NodeHandler constructor.
     *
     * @param string $distribution
     */
    private function __construct(string $distribution)
    {
        parent::__construct();

        $this->distribution = $distribution;
    }

    /**
     * @param string $distribution
     *
     * @return self
     */
    public static function create(string $distribution): self
    {
        return new static($distribution);
    }

    /**
     * @param array $config
     *
     * @return array
     * @throws ConfigurationMissingException
     */
    public function buildCommands(array $config): array
    {
        if (!empty($config[Constants::VERSION])) {
            try {
                $data = json_decode((string) file_get_contents('https://nodejs.org/dist/index.json'), true);
                foreach ($data as $version) {
                    if (Utils::keyExist(Constants::NPM, $version) &&
                        $version[Constants::VERSION] === 'v' . $config[Constants::VERSION]) {
                        $this->append('curl -sL https://deb.nodesource.com/setup_10.x | /bin/bash -');
                        $ap = AdvancedPackagingHandler::packagingInstall($this->distribution);
                        $this->append($ap . 'nodejs');
                        $this->append('npm install -g n');
                        $this->append('npm install -g npm@' . $version[Constants::NPM]);
                        $this->append('n ' . $config[Constants::VERSION]);

                        $packages = $config[Constants::PACKAGES];
                        if (!empty($packages)) {
                            $this->append('npm install -g ' . implode(' ', array_unique($packages)));
                        }
                    }
                }
            } catch (\Exception $exception) {
                throw ConfigurationMissingException::exception('Unable to load node versions');
            }
        }

        return $this->getValues();
    }
}
