<?php

namespace Fidesio\DockerfileComposer\Handler;

use Fidesio\DockerfileComposer\Utils\Constants;

/**
 * Class ServerHandler
 * @package Fidesio\DockerfileComposer\Handler
 */
final class ServerHandler extends AbstractHandler
{
    /**
     * @return self
     */
    public static function create(): self
    {
        return new static();
    }

    /**
     * @param array[] $config
     *
     * @return array
     */
    public function buildCommands(array $config): array
    {
        if (array_key_exists(Constants::DOCUMENT_ROOT, $config)) {
            $this->append(
                'sed -i "s|/var/www/html|/var/www/html' .
                $config[Constants::DOCUMENT_ROOT] .
                '|g" /etc/apache2/sites-available/000-default.conf'
            );
        }

        if (array_key_exists(Constants::MODULES, $config)) {
            $modules = $config[Constants::MODULES];
            foreach ($modules as $module) {
                $this->append('a2enmod ' . $module);
            }
        }

        return $this->getValues();
    }
}
