<?php

namespace Fidesio\DockerfileComposer\Handler;

/**
 * Class AdvancedPackagingHandler
 * @package Fidesio\DockerfileComposer\Handler
 */
final class AdvancedPackagingHandler extends AbstractHandler
{
    /**
     * @var string
     */
    private $distribution;

    /**
     * Configuration for the Advanced Packaging Tool - Linux
     */
    private const APT = [
        'echo \'debconf debconf/frontend select Noninteractive\' | debconf-set-selections',
        'apt-get update && apt-get upgrade -y apt-utils',
    ];

    /**
     * Configuration for the Alpine Linux Packages
     */
    private const APK = [
        'apk update && apk upgrade --available'
    ];

    /**
     * LinuxHandler constructor.
     *
     * @param string $distribution
     */
    private function __construct(string $distribution)
    {
        parent::__construct();

        $this->distribution = $distribution;
    }

    /**
     * @param string $distribution
     *
     * @return self
     */
    public static function create(string $distribution): self
    {
        return new static($distribution);
    }

    /**
     * @param array $commands
     *
     * @return array
     */
    public function commandsExtension(array &$commands): array
    {
        if (self::isAdvancedPackagingTool($this->distribution)) {
            $commands [] = 'apt-get purge -y --auto-remove';
            $commands [] = 'apt-get clean';
        } else {
            $commands [] = 'apk cache clean';
        }

        return $commands;
    }

    /**
     * @param array $packages
     *
     * @return array
     */
    public function buildCommands(array $packages): array
    {
        $this->appends(self::isAdvancedPackagingTool($this->distribution) ? self::APT : self::APK);
        $this->append(
            self::packagingInstall($this->distribution) . implode(
                ' \\' . PHP_EOL . '    ',
                array_unique($packages)
            )
        );

        return $this->getValues();
    }

    /**
     * @param string $platform
     *
     * @return string
     */
    public static function packagingInstall(string $platform): string
    {
        /** Configuration for the Advanced Packaging Tool */
        $apt = 'apt-get install -y --no-install-recommends ';

        /** Configuration for the Alpine Linux Packages */
        $apk = 'apk add --no-cache ';

        return self::isAdvancedPackagingTool($platform) ? $apt : $apk;
    }

    /**
     * @param string $platform
     *
     * @return bool
     */
    public static function isAdvancedPackagingTool(string $platform): bool
    {
        return is_bool(strpos($platform, 'alpine'));
    }
}
