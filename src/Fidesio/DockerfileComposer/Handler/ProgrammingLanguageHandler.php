<?php

namespace Fidesio\DockerfileComposer\Handler;

use Fidesio\DockerfileComposer\Exception\ConfigurationMissingException;
use Fidesio\DockerfileComposer\Utils\Constants;
use Fidesio\DockerfileComposer\Utils\Utils;

/**
 * Class ProgrammingLanguageHandler
 * @package Fidesio\DockerfileComposer\Handler
 */
final class ProgrammingLanguageHandler extends AbstractHandler
{
    /**
     * @var string
     */
    private $distribution;

    /**
     * @var array
     */
    private $extensions = [];

    /**
     * @var array
     */
    private $dependencies = [];

    /**
     * ProgrammingLanguageHandler constructor.
     *
     * @param string $distribution
     */
    private function __construct(string $distribution)
    {
        parent::__construct();

        $this->distribution = $distribution;
    }

    /**
     * @param string $distribution
     *
     * @return self
     */
    public static function create(string $distribution): self
    {
        return new static($distribution);
    }

    /**
     * @return array
     */
    public function getDependencies(): array
    {
        return $this->dependencies;
    }

    /**
     * @param array  $extensions
     * @param string $version
     *
     * @return void
     * @throws ConfigurationMissingException
     */
    public function processExtensionsDependencies(array $extensions, string $version): void
    {
        $phpConfig = json_decode(
            (string) file_get_contents('data/config/php/versions/' . $version . '.json'),
            true
        );

        foreach ($extensions as $extension) {
            $extension = $this->appendExtension($phpConfig, $extension);
            $this->appendDependency($extension);
        }
    }

    /**
     * @param array  $phpConfig
     * @param string $extension
     *
     * @return array
     * @throws ConfigurationMissingException
     */
    private function appendExtension(array $phpConfig, string $extension): array
    {
        if (!in_array($extension, $phpConfig[Constants::EXTENSIONS], true)) {
            throw ConfigurationMissingException::exception(
                'Missing extension ' . $extension . ' for PHP ' . $phpConfig[Constants::VERSION]
            );
        }

        $extension = json_decode(
            (string) file_get_contents('data/config/php/extensions/' . $extension . '.json'),
            true
        );

        $this->extensions[$extension['name']] = $extension;

        return $extension;
    }

    /**
     * @param array $extensions
     */
    private function appendDependency(array $extensions): void
    {
        $distribution = $this->typeDistribution();

        if (!empty($extensions[Constants::DEPENDENCIES])) {
            $dependencies = Utils::keyExist($distribution, $extensions[Constants::DEPENDENCIES]) ?
                $extensions[Constants::DEPENDENCIES][$distribution] :
                $extensions[Constants::DEPENDENCIES];
            foreach ($dependencies as $dependency) {
                $this->dependencies [] = $dependency;
            }
        }
    }

    /**
     * @param array $config
     *
     * @return array
     */
    public function buildCommands(array $config): array
    {
        $extensions = $this->getExtensions();

        foreach (array_keys($extensions) as $key) {
            $extension = $extensions[$key];
            $this->configureArguments($extension);
            $this->installArguments($extension);
            $this->peclArguments($extension);
            $this->enableArguments($extension);
        }

        $this->installComposer();

        return $this->getValues();
    }

    /**
     * @return array
     */
    public function getExtensions(): array
    {
        return $this->extensions;
    }

    /**
     * @param array $extension
     */
    private function configureArguments(array $extension): void
    {
        if (Utils::keyExist('configure-arguments', $extension) && is_array($extension['configure-arguments'])) {
            if (!empty($extension['configure-pre-commands'])) {
                $this->appends($extension['configure-pre-commands']);
            }
            if (!empty($extension['configure-arguments'])) {
                $this->append(
                    'docker-php-ext-configure ' . $extension['name'] . ' ' .
                    implode(' \\' . PHP_EOL . '    ', array_unique($extension['configure-arguments']))
                );
            }
            if (!empty($extension['configure-post-commands'])) {
                $this->appends($extension['configure-post-commands']);
            }
        }
    }

    /**
     * @param array $extension
     */
    private function installArguments(array $extension): void
    {
        if (Utils::keyExist('install-arguments', $extension) && is_array($extension['install-arguments'])) {
            if (!empty($extension['install-pre-commands'])) {
                $this->appends($extension['install-pre-commands']);
            }
            $options = implode(' ', array_unique($extension['install-arguments']));
            $this->append('docker-php-ext-install ' . ($options ? $options . ' ' : '') . $extension['name']);
            if (!empty($extension['install-post-commands'])) {
                $this->appends($extension['install-post-commands']);
            }
        }
    }

    /**
     * @param array $extension
     */
    private function peclArguments(array $extension): void
    {
        if (Utils::keyExist('pecl-arguments', $extension) && is_array($extension['pecl-arguments'])) {
            if (!empty($extension['pecl-pre-commands'])) {
                $this->appends($extension['pecl-pre-commands']);
            }
            $this->append('pecl install ' . implode(' ', array_unique($extension['pecl-arguments'])));
            if (!empty($extension['pecl-post-commands'])) {
                $this->appends($extension['pecl-post-commands']);
            }
        }
    }

    /**
     * @param array $extension
     */
    private function enableArguments(array $extension): void
    {
        if (Utils::keyExist('enable-arguments', $extension) && is_array($extension['enable-arguments'])) {
            if (!empty($extension['enable-pre-commands'])) {
                $this->appends($extension['enable-pre-commands']);
            }
            $options = implode(' ', array_unique($extension['enable-arguments']));
            $this->append('docker-php-ext-enable ' . $options . $extension['name']);
            if (!empty($extension['enable-post-commands'])) {
                $this->appends($extension['enable-post-commands']);
            }
        }
    }

    /**
     *
     */
    private function installComposer(): void
    {
        $this->append(
            'curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer'
        );
    }

    /**
     * @return string
     */
    private function typeDistribution(): string
    {
        return in_array(
            $this->distribution,
            Constants::DISTRIBUTIONS,
            true
        ) ? $this->distribution : 'debian';
    }
}
