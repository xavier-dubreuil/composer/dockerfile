<?php

namespace Fidesio\DockerfileComposer\Handler;

use Fidesio\DockerfileComposer\Exception\ConfigurationMissingException;
use Fidesio\DockerfileComposer\Model\Dockerfile;
use Fidesio\DockerfileComposer\Utils\Constants;
use Fidesio\DockerfileComposer\Utils\Utils;

/**
 * Trait InstructionsTrait
 * @package Fidesio\DockerfileComposer\Handler
 */
trait InstructionsTrait
{
    /**
     * @var Dockerfile
     */
    protected $dockerfile;

    /**
     * @var array
     */
    protected $configuration;

    /**
     * @var array
     */
    protected $instructions = [];

    /**
     * The FROM instruction initializes a new build stage and sets the Base Image for subsequent instructions.
     * As such, a valid Dockerfile must start with a FROM instruction.
     * The image can be any valid image – it is especially easy to start by pulling an image from the
     * Public Repositories.
     */
    protected function from(): void
    {
        $from = $this->dockerfile->getFrom();
        if (!empty($from)) {
            $this->appendInstruction(
                'FROM php:' . $from[Constants::VERSION] . '-' . $from[Constants::PLATFORM] .
                (!empty($from[Constants::DISTRIBUTION]) ? '-' . $from[Constants::DISTRIBUTION] : '')
            );
        }
    }

    /**
     * The LABEL instruction adds metadata to an image. A LABEL is a key-value pair. To include spaces within a LABEL
     * value, use quotes and backslashes as you would in command-line parsing.
     */
    protected function label(): void
    {
        $labels = $this->dockerfile->getLabel();
        if (!empty($labels)) {
            $this->instructions = array_merge(
                $this->instructions,
                Utils::multilines($this->labelLines($labels), 'LABEL', '')
            );
        }
    }

    /**
     * he RUN instruction will execute any commands in a new layer on top of the current image and commit the results.
     * The resulting committed image will be used for the next step in the Dockerfile.
     *
     * @throws ConfigurationMissingException
     */
    protected function run(): void
    {
        $from         = $this->dockerfile->getFrom();
        $extensions   = $from[Constants::EXTENSIONS];
        $distribution = $from[Constants::DISTRIBUTION] ?? 'debian';

        $serverHandler    = ServerHandler::create();
        $nodeHandler      = NodeHandler::create($distribution);
        $languageHandler  = ProgrammingLanguageHandler::create($distribution);
        $packagingHandler = AdvancedPackagingHandler::create($distribution);

        $languageHandler->processExtensionsDependencies($extensions, $from[Constants::VERSION]);
        $packages = $this->platformAndLanguagePackages(
            $this->dockerfile->getRun(),
            $languageHandler->getDependencies()
        );

        $packagingCommands   = $packagingHandler->buildCommands($packages);
        $programmingCommands = $languageHandler->buildCommands($extensions);
        $server              = $this->configuration[Constants::APACHE] ?? [];
        $serverCommands      = $serverHandler->buildCommands($server);
        $node                = $this->configuration[Constants::NODE] ?? [];
        $nodeCommands        = $nodeHandler->buildCommands($node);

        $commands = array_merge($packagingCommands, $programmingCommands, $serverCommands, $nodeCommands);

        $packagingHandler->commandsExtension($commands);

        $this->instructions = array_merge(
            $this->instructions,
            Utils::multilines($commands, Constants::RUN, '&&')
        );
    }

    /**
     * The ARG instruction defines a variable that users can pass at build-time to the builder with the
     * docker build command using the --build-arg <varname>=<value> flag. If a user specifies a build
     * argument that was not defined in the Dockerfile, the build outputs a warning.
     */
    protected function arguments(): void
    {
        $arguments = $this->dockerfile->getArguments();
        if (!empty($arguments)) {
            $this->appendInstruction('');
            foreach ($arguments as $argument) {
                $this->appendInstruction('ARG ' . $argument);
            }
        }
    }

    /**
     * The ENV instruction sets the environment variable <key> to the value <value>.
     * This value will be in the environment for all subsequent instructions in the
     * build stage and can be replaced inline in many as well.
     */
    protected function environment(): void
    {
        $environment = $this->dockerfile->getEnvironment();
        if (!empty($environment)) {
            $this->appendInstruction('');
            $this->appendInstruction(
                'ENV ' . implode(' \\' . PHP_EOL . '    ', array_unique($environment)) . PHP_EOL
            );
        }
    }

    /**
     * The VOLUME instruction creates a mount point with the specified name and marks it as holding
     * externally mounted volumes from native host or other containers.
     */
    protected function volume(): void
    {
        $volumes = $this->dockerfile->getVolume();
        if (!empty($volumes)) {
            $this->appendInstruction(
                PHP_EOL . 'VOLUME ' . '[' . '"' . implode('", "', $volumes) . '"' . ']'
            );
        }
    }

    /**
     * The WORKDIR instruction sets the working directory for any RUN, CMD,
     * ENTRYPOINT, COPY and ADD instructions that follow it in the Dockerfile
     */
    protected function workdir(): void
    {
        $workdir = $this->dockerfile->getWorkdir();
        if (!empty($workdir)) {
            $this->appendInstruction(PHP_EOL . 'WORKDIR ' . $workdir);
        }
    }

    /**
     * ENTRYPOINT allows you to configure a container that will run as an executable.
     * Only the last ENTRYPOINT instruction in the Dockerfile will have an effect.
     */
    protected function entrypoint(): void
    {
        $entrypoint = $this->dockerfile->getEntrypoint();
        if (!empty($entrypoint)) {
            $this->appendInstruction(
                PHP_EOL . 'ENTRYPOINT ' . '[' . '"' . implode('", "', $entrypoint) . '"' . ']'
            );
        }
    }

    /**
     * There can only be one CMD instruction in a Dockerfile.
     * If you list more than one CMD then only the last CMD will take effect.
     * The main purpose of a CMD is to provide defaults for an executing container. These defaults can include an
     * executable, or they can omit the executable, in which case you must specify an ENTRYPOINT instruction as well.
     */
    protected function cmd(): void
    {
        $cmd = $this->dockerfile->getCmd();
        if (!empty($cmd)) {
            $this->appendInstruction(
                PHP_EOL . 'CMD ' . '[' . '"' . implode('", "', $cmd) . '"' . ']'
            );
        }
    }

    /**
     * The EXPOSE instruction informs Docker that the container listens on the specified network ports at runtime.
     * You can specify whether the port listens on TCP or UDP, and the default is TCP if the protocol is not specified.
     */
    protected function expose(): void
    {
        $expose = $this->dockerfile->getExpose();
        if (!empty($expose)) {
            foreach ($expose as $value) {
                $this->appendInstruction('EXPOSE ' . $value);
            }
            $this->appendInstruction('');
        }
    }

    /**
     * @param array[] $copy
     * @param string  $cmd
     */
    protected function postCommand(array $copy, string $cmd): void
    {
        if (!empty($copy[Constants::TARGET]) && !empty($copy[Constants::SOURCES])) {
            $this->appendInstruction('');
            if (is_iterable($copy[Constants::SOURCES])) {
                if (isset($copy[Constants::WITH_NAME]) && $copy[Constants::WITH_NAME]) {
                    foreach ($copy[Constants::SOURCES] as $source) {
                        $finalName = explode('/', $source);
                        $this->appendInstruction(
                            $cmd . ' ' . $source . ' ' . rtrim((string) $copy[Constants::TARGET], '/') .
                            '/' . $finalName[count($finalName) - 1]
                        );
                    }
                } else {
                    $this->appendInstruction(
                        $cmd . ' ' . implode(' ', (array) $copy[Constants::SOURCES]) . ' ' .
                        rtrim((string)$copy[Constants::TARGET], '/') . '/'
                    );
                }
            } else {
                $this->appendInstruction(
                    $cmd . ' ' . $copy[Constants::SOURCES] . ' ' . $copy[Constants::TARGET]
                );
            }
        }
    }

    /**
     * @param array $run
     */
    protected function postCommandRun(array $run): void
    {
        if (!empty($run[Constants::COMMANDS])) {
            $this->appendInstruction('');
            $this->instructions = array_merge(
                $this->instructions,
                Utils::multilines($run[Constants::COMMANDS], 'RUN', '&&')
            );
        }
    }

    /**
     * @param array $first
     * @param array $second
     *
     * @return array
     */
    protected function platformAndLanguagePackages(array $first, array $second): array
    {
        return array_merge($first, $second);
    }

    /**
     * @param string $instruction
     *
     * @return self
     */
    protected function appendInstruction(string $instruction): self
    {
        $this->instructions [] = $instruction;

        return $this;
    }

    /**
     * @param array $labels
     *
     * @return array
     */
    protected function labelLines(array $labels): array
    {
        $tags = [];
        foreach ($labels as $label => $value) {
            $tags [] = $label . '="' . $value . '"';
        }

        return $tags;
    }
}
