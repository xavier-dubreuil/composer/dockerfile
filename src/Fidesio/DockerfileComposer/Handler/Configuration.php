<?php

namespace Fidesio\DockerfileComposer\Handler;

use Fidesio\DockerfileComposer\Exception\ConfigurationMissingException;
use Fidesio\DockerfileComposer\Exception\FileNotCreatedException;
use Fidesio\DockerfileComposer\Interfaces\ConfigurationInterface;
use Fidesio\DockerfileComposer\Utils\Constants;
use Fidesio\DockerfileComposer\Utils\Utils;

/**
 * Class Configuration
 * @package Fidesio\DockerfileComposer\Handler
 */
final class Configuration implements ConfigurationInterface
{
    /**
     * Chemin de configuration pour la version de PHP
     */
    private const PATH_VERSIONS = 'data/config/php/versions/';

    /**
     * @var string
     */
    private $filename;

    /**
     * @var array
     */
    private $config;

    /**
     * ConfigurationFile constructor.
     *
     * @param string $filename
     */
    private function __construct(string $filename)
    {
        $this->config   = [];
        $this->filename = $filename;
    }

    /**
     * @param string $filename
     *
     * @return self
     */
    public static function create(string $filename): self
    {
        return new static($filename);
    }

    /**
     * @return array
     *
     * @throws ConfigurationMissingException
     * @throws FileNotCreatedException
     */
    public function getContents(): array
    {
        $this->ensureFileExists();
        $this->ensureIsValidJson();
        $this->ensureIsValidImage();
        $this->ensureIsValidLanguageSettings();

        return $this->config;
    }

    /**
     * @throws FileNotCreatedException
     */
    private function ensureFileExists(): void
    {
        if (!file_exists($this->filename)) {
            throw FileNotCreatedException::exception('Missing configuration file ' . $this->filename);
        }
    }

    /**
     * @throws ConfigurationMissingException
     */
    private function ensureIsValidJson(): void
    {
        $this->config = json_decode((string) file_get_contents($this->filename), true);

        // switch and check possible JSON errors
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                $error = '';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Invalid or malformed JSON.';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Control character error, possibly incorrectly encoded.';
                break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON.';
                break;
            default:
                $error = 'Unknown JSON error occured.';
                break;
        }

        if ($error !== '') {
            throw ConfigurationMissingException::exception($error);
        }
    }

    /**
     * @throws ConfigurationMissingException
     */
    private function ensureIsValidImage(): void
    {
        if (empty($this->config[Constants::PHP])) {
            throw ConfigurationMissingException::exception('Missing php in configuration file');
        }
        if (empty($this->config[Constants::PHP][Constants::VERSION])) {
            throw ConfigurationMissingException::exception('Missing php version in configuration file');
        }
        if (empty($this->config[Constants::PHP][Constants::PLATFORM])) {
            throw ConfigurationMissingException::exception('Missing platform in configuration file');
        }
    }

    /**
     * @throws ConfigurationMissingException
     */
    private function ensureIsValidLanguageSettings(): void
    {
        $version  = $this->config[Constants::PHP][Constants::VERSION];
        $settings = $this->languageSettings($version);
        $platform = $this->config[Constants::PHP][Constants::PLATFORM];

        if (!empty($this->config[Constants::APACHE]) && !$this->isApachePlatform($platform)) {
            throw ConfigurationMissingException::exception('Apache configuration is not valid');
        }
        if (!Utils::keyExist($platform, $settings[Constants::PLATFORMS])) {
            throw ConfigurationMissingException::exception('Unkonwn platform ' . $platform);
        }
        if (!in_array($version, array_values($settings), true)) {
            throw ConfigurationMissingException::exception('Unknown PHP version ' . $version);
        }
        if (!empty($this->config[Constants::PHP][Constants::DISTRIBUTION])) {
            $distribution = $this->config[Constants::PHP][Constants::DISTRIBUTION];
            if (!in_array($distribution, $settings[Constants::PLATFORMS][$platform], true)) {
                throw ConfigurationMissingException::exception('Unknown distribution');
            }
        }
    }

    /**
     * @param string $version
     *
     * @return array
     */
    private function languageSettings(string $version): array
    {
        return json_decode((string) file_get_contents(self::PATH_VERSIONS . $version . '.json'), true);
    }

    /**
     * @param string $platform
     *
     * @return bool
     */
    private function isApachePlatform(string $platform): bool
    {
        return is_int(strpos($platform, Constants::APACHE));
    }
}
