<?php

namespace Fidesio\DockerfileComposer\Handler;

use Fidesio\DockerfileComposer\Exception\ConfigurationMissingException;
use Fidesio\DockerfileComposer\Exception\FileNotCreatedException;
use Fidesio\DockerfileComposer\Interfaces\FormatterInterface;
use Fidesio\DockerfileComposer\Model\Dockerfile;
use Fidesio\DockerfileComposer\Utils\Constants;

/**
 * Class StringFormatter
 * @package Fidesio\DockerfileComposer\Handler
 */
class StringFormatter implements FormatterInterface
{
    use InstructionsTrait;

    /**
     * FormatterHandler constructor.
     *
     * @param Dockerfile $dockerfile
     * @param array      $configuration
     */
    private function __construct(Dockerfile $dockerfile, array $configuration)
    {
        $this->dockerfile    = $dockerfile;
        $this->configuration = $configuration;
    }

    /**
     * @param string $filename
     *
     * @return self
     * @throws ConfigurationMissingException
     * @throws FileNotCreatedException
     */
    public static function create(string $filename): self
    {
        $dockerfileHandler = DockerfileHandler::create();
        $dockerfile        = $dockerfileHandler->processConfig($filename);
        $configuration     = $dockerfileHandler->getConfiguration();

        return new static($dockerfile, $configuration);
    }

    /**
     * @return string
     * @throws ConfigurationMissingException
     */
    public function format(): string
    {
        $this->from();
        $this->label();
        $this->arguments();
        $this->environment();
        $this->expose();
        $this->run();
        $this->postCommands();
        $this->workdir();
        $this->volume();
        $this->entrypoint();
        $this->cmd();

        $this->instructions[] = '';

        return implode(PHP_EOL, $this->instructions);
    }

    /**
     * The COPY instruction copies new files or directories from <src>
     * and adds them to the filesystem of the container at the path <dest>
     */
    private function postCommands(): void
    {
        $copies = $this->dockerfile->getCopy();

        foreach ($copies as $copy) {
            if ($copy[Constants::CMD] === Constants::COPY) {
                $this->postCommand($copy, Constants::COPY);
            }
            if ($copy[Constants::CMD] === Constants::RUN) {
                $this->postCommandRun($copy);
            }
            if ($copy[Constants::CMD] === Constants::ADD) {
                $this->postCommand($copy, Constants::ADD);
            }
        }
    }
}
