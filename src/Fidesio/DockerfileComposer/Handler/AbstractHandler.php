<?php

namespace Fidesio\DockerfileComposer\Handler;

/**
 * Class AbstractHandler
 * @package Fidesio\DockerfileComposer\Handler
 */
abstract class AbstractHandler
{
    /**
     * @var array
     */
    private $commands;

    /**
     * AbstractHandler constructor.
     */
    protected function __construct()
    {
        $this->commands = [];
    }

    /**
     * @return array
     */
    public function getValues(): array
    {
        return $this->commands;
    }

    /**
     * @param array $commands
     */
    public function add(array $commands): void
    {
        $this->commands = $commands;
    }

    /**
     * Add command in array
     *
     * @param string $command
     */
    public function append(string $command): void
    {
        $this->commands [] = $command;
    }

    /**
     * @param array $commands
     */
    public function appends(array $commands): void
    {
        foreach ($commands as $command) {
            $this->commands [] = $command;
        }
    }

    /**
     * @param array $config
     *
     * @return array
     */
    abstract public function buildCommands(array $config): array;
}
