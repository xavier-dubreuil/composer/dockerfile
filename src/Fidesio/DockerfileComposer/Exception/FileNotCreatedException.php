<?php

namespace Fidesio\DockerfileComposer\Exception;

/**
 * Class FileException
 * @package Fidesio\DockerfileComposer\Exception
 */
class FileNotCreatedException extends \Exception
{
    /**
     * FileException constructor.
     *
     * @param string $message
     */
    private function __construct(string $message)
    {
        parent::__construct($message);
    }

    /**
     * @param string $message
     *
     * @return  self
     */
    public static function exception(string $message): self
    {
        return new static(
            $message
        );
    }
}
