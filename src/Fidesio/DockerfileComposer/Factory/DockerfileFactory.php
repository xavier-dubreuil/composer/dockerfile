<?php

namespace Fidesio\DockerfileComposer\Factory;

use Fidesio\DockerfileComposer\Exception\ConfigurationMissingException;
use Fidesio\DockerfileComposer\Exception\FileNotCreatedException;
use Fidesio\DockerfileComposer\Handler\StringFormatter;
use Fidesio\DockerfileComposer\Interfaces\FactoryInterface;
use RuntimeException;

/**
 * Class DockerfileBuild
 * @package Fidesio\DockerfileComposer\Factory
 */
class DockerfileFactory implements FactoryInterface
{
    /**
     * @param string $fileName
     * @param string $output
     *
     * @throws FileNotCreatedException
     * @throws ConfigurationMissingException
     */
    public static function generate(string $fileName, string $output): void
    {
        self::verifyDirectory($output);

        $file = fopen($output . '/' . 'Dockerfile', 'wb');
        if (!is_bool($file)) {
            fwrite($file, StringFormatter::create($fileName)->format());
            fclose($file);
        }
    }

    /**
     * @param string $output
     */
    private static function verifyDirectory(string $output): void
    {
        if (!file_exists($output) && !mkdir($output, 0777, true) && !is_dir($output)) {
            throw new RuntimeException(
                sprintf(
                    'The output directory cannot be accessed "%s" was not created',
                    $output
                )
            );
        }
    }
}
