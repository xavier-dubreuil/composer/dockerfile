# Dockerfile Generator

[![pipeline status](https://git.fidesio.com/fidesio-rnd/dockerfile-composer/badges/master/pipeline.svg?style=flat-square)](https://git.fidesio.com/fidesio-rnd/dockerfile-composer/commits/master)
[![coverage report](https://git.fidesio.com/fidesio-rnd/dockerfile-composer/badges/master/coverage.svg?style=flat-square)](https://git.fidesio.com/fidesio-rnd/dockerfile-composer/commits/master)

Dockerfile generator written in PHP and installable with composer or from phar

## What's it about?

As PHP developer using docker, you need to build your Dockerfile every time you have a new project.

Sometimes you don't know which system libs are required for a specific extension you're installing or which lib to use when working with alpine / stretch.

Imagine you want to install php-gd. You need to install:

-For "**debian**": `libfreetype6-dev`, `libjpeg62-turbo-dev`, `libpng-dev`
-For "**alpine**": `libfreetype-dev`, `libjpeg-turbo-dev`, `libpng-dev`

With this tool, you don't need to know or search which packages should be installed.

This CLI-Tool parse the json file you give to build your Dockerfile with the most accurate style at the time of the writing.

The generated Dockerfile could be directly used in devops systems like Azure DevOps / Amazon Web Services / ... 

## Installation / Usage

Docker file composer is not supposed to be installed as part of your project dependencies.
  
### PHAR file [preferred]

Please check the [releases](https://git.fidesio.com/fidesio-rnd/dockerfile-composer/releases) for available phar files.
Download the latest release and and run it like this:
```
php dockerfile-composer.phar generate /path/to/your/dockerfile-config.json docker/php/
```

### Composer - global command

This package can be easily globally installed by using [Composer]:

```sh
composer require --dev fidesio/dockerfile-composer
```

If this is already done, run it like this:

```
vendor/bin/dockerfile-composer generate /path/to/your/dockerfile-config.json docker/php/
```

## Configuration

Dockerfile Composer need two information as input:

- A config file: have a look at the [config file example](data/dockerfile-config.sample.json) to see which configuration options are available.
You can adjust this file, as needed.
- An output directory: the directory in which to `Dockerfile` should be put.

**Notice: Every time the command is executed, the generation is recreated. You should never change the content directly**

```sh
bin/composer-require-checker check --config-file=path/to/config.json /path/to/your/project/composer.json
bin/dockerfile-composer generate dockerfile-config.json docker/php/
``` 

## Credits

This package was initially designed by [Xavier Dubreuil](https://git.fidesio.com/xavier.dubreuil) in Python.
This version is mainly a port from the Python version.

Big thanks to all [Contributors](https://git.fidesio.com/fidesio-rnd/dockerfile-composer/graphs/master).

[Composer]: https://getcomposer.org
